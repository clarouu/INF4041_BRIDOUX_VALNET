package premiereapplication.automation.test.manageyourmoneyappli;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.NotificationCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Logger;

public class MainActivity extends AppCompatActivity {

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    /*public static final String BIERS_UPDATE = "com.octip.cours.inf4042_11.BIERS_UPDATE";
    private static final String TAG = "Main";
    public RecyclerView rv;*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Button buttonS = (Button) findViewById(R.id.button3) ;
        buttonS.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //GetBiersServices.startAction_get_all_biers(MainActivity.this);
                //notification(1);
                Intent intentR = new Intent(MainActivity.this, recyclerView.class);
                startActivity(intentR);
            }
        });

       /* Button button = (Button) findViewById(R.id.button5);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
               GetBiersServices.startAction_get_all_biers(MainActivity.this);
               notification(1);
            }
        });*/

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu); //pour récupérer le bon fichier xml
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //sait déjà que j'ai cliqué sur quelque chose
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.toast_me) {
            Toast.makeText(getApplicationContext(),(R.string.msg_toast),
                    Toast.LENGTH_LONG).show();

        }
        if (id == R.id.info_details) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this); //this = le context = l'activity
            builder.setMessage(R.string.dialog_message).setTitle(R.string.dialog_title);
            AlertDialog dialog = builder.create();
            dialog.show();//afficher la pop-up

        }
        if (id == R.id.intent) {
            Intent intent = new Intent(this, DisplayMessageActivity.class);
            startActivity(intent);
        }


        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
