package premiereapplication.automation.test.manageyourmoneyappli;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.NotificationCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class recyclerView extends AppCompatActivity {

    public static final String BIERS_UPDATE = "com.octip.cours.inf4042_11.BIERS_UPDATE";
    private static final String TAG = "recyclerV";
    public RecyclerView rv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_view);

        IntentFilter intentFilter = new IntentFilter(BIERS_UPDATE);//
        LocalBroadcastManager.getInstance(this).registerReceiver(new BierUpdate(), intentFilter);//

        rv = (RecyclerView)findViewById(R.id.rv_biere);
        rv.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        rv.setAdapter(new BiersAdapter(getBiersFromFile()));
    }

    public void startService(View v) {

        GetBiersServices.startAction_get_all_biers(this);
    }

    public JSONArray getBiersFromFile(){
        try {
            InputStream is = new FileInputStream(getCacheDir() + "/" + "bieres.json");
            byte[] buffer = new byte[is.available()];
            is.read(buffer);
            is.close();
            JSONArray result = new JSONArray(new String(buffer, "UTF-8"));
            Log.d(TAG,result.toString());
            return result;
        } catch (IOException e) {
            e.printStackTrace();
            return new JSONArray();
        } catch (JSONException e) {
            e.printStackTrace();
            return new JSONArray();
        }
    }

    public void notification(int n){
            Intent resultIntent = new Intent(this, BierUpdate.class);
// Because clicking the notification opens a new ("special") activity, there's
// no need to create an artificial back stack.
            PendingIntent resultPendingIntent =
                    PendingIntent.getActivity(
                            this,
                            0,
                            resultIntent,
                            PendingIntent.FLAG_UPDATE_CURRENT
                    );
            android.support.v4.app.NotificationCompat.Builder mBuilder = null;
            if(n==1){
                mBuilder =
                        new NotificationCompat.Builder(this).setSmallIcon(R.drawable.star)
                                .setContentTitle("Notification").setContentText("Téléchargement en cours...");
            }else if(n==2){
                mBuilder =
                        new NotificationCompat.Builder(this).setSmallIcon(R.drawable.target)
                                .setContentTitle("Notification").setContentText("Téléchargement terminé !");
            }

// Sets an ID for the notification
            int mNotificationId = 5;
// Gets an instance of the NotificationManager service
            NotificationManager mNotifyMgr =
                    (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
// Builds the notification and issues it.
            if(mBuilder != null) {
                mNotifyMgr.notify(mNotificationId, mBuilder.build());
            }
        }

    public void updateBiers() {
        getBiersFromFile();
    }

    public class BierUpdate extends BroadcastReceiver {

        private static final String TAG = "BierUpdate";

        public void onReceive(Context context, Intent intent){
            //notification que le service a terminé le télécharchement
            Log.d(TAG,getIntent().getAction());
            updateBiers();
            notification(2);
            ((BiersAdapter)rv.getAdapter()).setNewBiere(getBiersFromFile());
        }
    }
}
