package premiereapplication.automation.test.manageyourmoneyappli;

import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Alina on 21/11/2016.
 */
public class BiersAdapter extends RecyclerView.Adapter<BiersAdapter.BierHolder>{

    private JSONArray biers;

    public BiersAdapter(JSONArray jsonArray){

        this.biers = jsonArray;
    }
    @Override
    public BierHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater lf = LayoutInflater.from(parent.getContext());
        View v = lf.inflate(R.layout.rv_bier_element, parent, false);
        BierHolder sh = new BierHolder(v);
        return sh;
    }

    @Override
    public void onBindViewHolder(BierHolder holder, int position) {

        try {
            JSONObject obj = (JSONObject) biers.get(position);
            holder.bierName.setText(obj.getString("name"));
            holder.bierDescription.setText(obj.getString("description"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {

        return biers.length();
    }

    public void setNewBiere(JSONArray jsonArray){
        notifyDataSetChanged();
    }

    public class BierHolder extends RecyclerView.ViewHolder{

        public TextView bierName;
        public TextView bierDescription;
        public BierHolder(View itemView) {
            super(itemView);
            bierName = (TextView) itemView.findViewById(R.id.rv_bier_element_name);
            bierDescription = (TextView) itemView.findViewById(R.id.rv_bier_element_description);
        }
    }
}

